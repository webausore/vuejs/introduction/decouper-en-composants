# vuejs-decouper-en-composants

Découper en composant Vue js en reprenant l'exemple de react

Preview : https://vuejs-decouper-en-composant.netlify.app/

Consignes :

A partir de l'illustration suivante (issue de la doc React) :

![composant-react](./react-component.png)

-   Faire une liste des possibles composants à utiliser
-   Créer ces composants en vuejs (utiliser pour l'instant la synthaxe template qui est présenté dans l'ex de la doc vue)
-   Reconstruire en code ce que tu vois sur l'image, en utilisant les composants précédemment créés et en utilisant les données fournie dans data.json
-   Sur l'image, les produits en rouge sont en rupture de stock (voir data.json)
-   Mettre le style de coté, on s'occupe juste de la logique vuejs
-   Tu n'es pas obligé de faire fonctionner la recherche et la case à cocher mais si tu te sens capable hésite pas

Aide : pour reproduire le contenu de l'image, tu auras besoin de cette aide : https://stackoverflow.com/a/47385953
