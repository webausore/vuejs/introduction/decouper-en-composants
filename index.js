Vue.component("search-bloc", {
  name: "search-bloc",
  template: `<form>
    <input type='text' placeholder='Search'/></br>
    <input type='checkbox' id='checkbox'/><label for='checkbox'>Only show products in stock</label>
    </form>`,
});

Vue.component("category-row", {
  name: "category-row",
  props: ["category"],
  template: "<p>{{ category }}</p>",
});

Vue.component("product-row", {
  name: "product-row",
  props: ["product"],
  template:
    "<div class='row'><p>{{ product.name }}</p><p>{{ product.price }}</p></div>",
});

Vue.component("products-bloc", {
  name: "products-bloc",
  props: ["products"],
  template: `
  <div>

    <div class='row'><p>Name</p><p>Price</p></div>

    <div v-for="(categories, category) in products">
      <category-row v-bind:category='category'></category-row>

      <product-row 
      v-for="product in categories"
      v-bind:product="product"
      v-bind:key="product.name">
      </product-row>
    </div>

  </div>`,
});

let app = new Vue({
  el: "#app",

  data: {
    products: [],
  },

  beforeCreate: function () {
    fetch("./data.json")
      .then((res) => res.json())
      .then((data) => {
        this.products = this.groupByKey(data, "category");
      });
  },

  methods: {
    groupByKey(list, key) {
      return list.reduce(
        (hash, obj) => ({
          ...hash,
          [obj[key]]: (hash[obj[key]] || []).concat(obj),
        }),
        {}
      );
    },
  },
});
