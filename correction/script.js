/**
 * Body (table)
 * Filter
 * Category
 * Product
 */

Vue.component('app-filter', {
	template: `
		<form>
			<div class="form-row">
				<input type="text" id="search" name="search" placeholder="Search" />
			</div>
			<div class="form-row">
				<input type="checkbox" id="in_stock" name="in_stock" />
				<label name="in_stock">Only show products in stock</label>
			</div>
		</form>
	`,
});

Vue.component('app-category', {
	template: '<tr><th>{{category}}</th></tr>',
	props: ['category'],
});

Vue.component('app-product', {
	template: `
		<tr>
			<td>{{product.name}}</td>
			<td>{{'$' + product.price}}</td>
		</tr>
	`,
	props: ['product'],
});

Vue.component('app-table', {
	template: `
		<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Price</th>
				</tr>
			</thead>
			<tbody>
				<slot />
			</tbody>
		</table>
	`,
});

const app = new Vue({
	el: '#app',
	data: {
		products: [],
	},
	beforeCreate() {
		fetch('./data.json')
			.then((res) => res.json())
			.then((data) => {
				this.products = this.groupByKey(data, 'category');
			});
	},
	methods: {
		groupByKey(list, key) {
			return list.reduce(
				(hash, obj) => ({
					...hash,
					[obj[key]]: (hash[obj[key]] || []).concat(obj),
				}),
				{}
			);
		},
	},
});
